package com.milogilad.towntp;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class TownTP extends JavaPlugin {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage("The console can't use this command!");
			return false;
		}

		if (cmd.getName().equalsIgnoreCase("setwarp")) {
			if (!sender.hasPermission("towntp.setwarp")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			setWarp(args[0], ((Player) sender).getLocation());
			sender.sendMessage("Set the warp '" + args[0] + "' to your current location");
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("warp")) {
			if (!sender.hasPermission("towntp.warp")) {
				sender.sendMessage("You're not permitted to use that command!");
				return false;
			}

			Location warp = getWarp(args[0]);

			if (warp == null) {
				sender.sendMessage("That warp does not exist!");
				return false;
			}

			((Player) sender).teleport(warp);
			sender.sendMessage("Teleported you to warp '" + args[0] + "'");
			return true;
		}

		return false;
	}

	private Location getWarp(String name) {
		try {
			return (Location) getConfig().get(name);
		} catch (Exception e) {
			return null;
		}
	}

	private void setWarp(String name, Location l) {
		getConfig().set(name, l);
		saveConfig();
	}
}
